package com.erlanmomo.grpc.trainee.ssp.mock.servlets;

import com.erlanmomo.grpc.trainee.ssp.mock.utils.BidResponseService;
import org.eclipse.jetty.http.HttpHeader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.stream.Collectors;

public class AdRequestServlet extends HttpServlet {
    private static final String JSON_TYPE = "application/json";
    private static final HttpClient httpClient = HttpClient.newHttpClient();

    @Override
    protected void doPost(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws ServletException, IOException {
        String adRequestBody = request.getReader().lines()
                .collect(Collectors.joining());
        HttpRequest bidRequest = HttpRequest.newBuilder()
                .header(HttpHeader.CONTENT_TYPE.asString(), JSON_TYPE)
                .POST(HttpRequest.BodyPublishers.ofByteArray(adRequestBody.getBytes()))
                .build();

        httpClient.sendAsync(bidRequest, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenApply(str -> {
                    BidResponseService.onBidResponse(str);
                    return str;
                })
                .thenApply(str -> {
                    try {
                        response.setStatus(200);
                        response.getWriter().write(str);
                    } catch (IOException e) {
                        response.setStatus(500);
                        e.printStackTrace();
                    }
                    return response;
                })
                .join();
    }
}
