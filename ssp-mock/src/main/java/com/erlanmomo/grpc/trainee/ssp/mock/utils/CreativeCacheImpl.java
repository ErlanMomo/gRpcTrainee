package com.erlanmomo.grpc.trainee.ssp.mock.utils;

import com.erlanmomo.grpc.trainee.common.entities.Creative;
import com.erlanmomo.grpc.trainee.grpc.CreativeGrpc;
import com.erlanmomo.grpc.trainee.grpc.CreativeServiceCatGrpc;
import com.erlanmomo.grpc.trainee.grpc.CreativeStatusUpdate;
import com.erlanmomo.grpc.trainee.grpc.Request;
import com.erlanmomo.grpc.trainee.grpc.Response;
import com.erlanmomo.grpc.trainee.ssp.mock.creatives.CreativeCache;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

public class CreativeCacheImpl implements CreativeCache {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreativeCacheImpl.class);
    private static final String CREATIVE_NEW_STATUS = "NEW";
    private static final HashMap<String, Creative> creatives = new HashMap<>();

    private CreativeServiceCatGrpc.CreativeServiceCatStub stub;

    public CreativeCacheImpl() {
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", 9090)
                .usePlaintext()
                .build();

        stub = CreativeServiceCatGrpc.newStub(channel);
        LOGGER.info("Read all creatives");
        stub.getAllCreatives(Request.newBuilder().build(), new StreamObserver<>() {
            @Override
            public void onNext(CreativeGrpc creativeGrpc) {
                Creative creative = new Creative(creativeGrpc.getCrid(),
                        creativeGrpc.getStatus(),
                        creativeGrpc.getTag());
                LOGGER.info("Get creative {}", creative);
                creatives.put(hash(creative), creative);
            }

            @Override
            public void onError(Throwable throwable) {
                LOGGER.error("Error during getting all creatives", throwable);
            }

            @Override
            public void onCompleted() {
                LOGGER.info("Complete getting all creatives");
            }
        });
        stub.subscribeCreativeStatusUpdate(Request.newBuilder().build(), new StreamObserver<>() {
            @Override
            public void onNext(CreativeStatusUpdate creativeStatusUpdate) {
                LOGGER.info("Update creative {} status {}", creativeStatusUpdate.getCrid(), creativeStatusUpdate.getStatus());
                if (creatives.containsKey(creativeStatusUpdate.getCrid())) {
                    Creative creative = creatives.get(creativeStatusUpdate.getCrid());
                    creative.setStatus(creativeStatusUpdate.getStatus());
                }
            }

            @Override
            public void onError(Throwable throwable) {
                LOGGER.info("Error", throwable);
            }

            @Override
            public void onCompleted() {
                LOGGER.info("Complete");
            }
        });
    }

    @Override
    public String hash(Creative creative) {
        return creative.getCrid();
    }

    @Override
    public Creative getByHash(String hash) {
        return creatives.get(hash);
    }

    @Override
    public Creative get(String crid, String tag) {
        Creative creative = new Creative(crid, CREATIVE_NEW_STATUS, tag);
        String hash = this.hash(creative);
        if (creatives.containsKey(hash)) {
            return creatives.get(hash);
        }
        onNewCreative(creative);
        creatives.put(crid, creative);
        return creative;
    }

    private void onNewCreative(Creative creative) {
        if (creatives.containsKey(creative.getCrid())) {
            return;
        }
        creative.setStatus(CREATIVE_NEW_STATUS);
        creatives.put(creative.getCrid(), creative);
        CreativeGrpc creativeGrpc = CreativeGrpc.newBuilder()
                .setCrid(creative.getCrid())
                .setTag(creative.getCrid())
                .build();

        stub.onNewCreative(creativeGrpc, new StreamObserver<>() {
            @Override
            public void onNext(Response response) {
            }

            @Override
            public void onError(Throwable throwable) {
            }

            @Override
            public void onCompleted() {
            }
        });
    }
}
