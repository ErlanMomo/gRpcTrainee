package com.erlanmomo.grpc.trainee.ssp.mock.utils;

import com.erlanmomo.grpc.trainee.common.entities.Creative;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.eclipse.jetty.util.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;

public class BidResponseService {
    private static final Logger LOGGER = LoggerFactory.getLogger(BidResponseService.class);
    private static final CreativeCacheImpl CREATIVE_CACHE_IMPL = new CreativeCacheImpl();

    private static final Gson GSON = new Gson();

    public BidResponseService() {
    }

    public static void onBidResponse(String bidResponse) {
        if(StringUtils.isEmpty(bidResponse)){
            LOGGER.debug("Empty bid response");
            return;
        }
        JsonElement jsonBidResponse = GSON.toJsonTree(bidResponse);
        getCreativesFromBidResponse(jsonBidResponse);
    }

    private static List<Creative> getCreativesFromBidResponse(JsonElement bidResponse) {
        List<Creative> creativeLinkedList = new LinkedList<>();
        JsonArray seatbids =
                bidResponse.getAsJsonObject().getAsJsonArray("seatbid");
        for (JsonElement seatbid : seatbids) {
            JsonArray bids = seatbid.getAsJsonObject().getAsJsonArray("bid");
            for (JsonElement bid : bids) {
                JsonObject bidObject = bid.getAsJsonObject();
                String crid = bidObject.get("crid").getAsString();
                String tag = bidObject.get("adm").getAsString();
                Creative creative = CREATIVE_CACHE_IMPL.get(crid, tag);
                creativeLinkedList.add(creative);
            }
        }
        return creativeLinkedList;
    }
}
