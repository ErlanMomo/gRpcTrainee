package com.erlanmomo.grpc.trainee.ssp.mock.servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BlockingServlet extends HttpServlet {
    private static final String JSON_TYPE = "application/json";
    @Override
    protected void doGet(
            HttpServletRequest request,
            HttpServletResponse response
    ) throws ServletException, IOException {
        System.out.println("Servlet working");
        response.setContentType(JSON_TYPE);
        response.setStatus(HttpServletResponse.SC_OK);
        response.getWriter().println("\"status\":\"ok\"");
    }
}
