package com.erlanmomo.grpc.trainee.ssp.mock.server;


import com.erlanmomo.grpc.trainee.ssp.mock.servlets.AdRequestServlet;
import com.erlanmomo.grpc.trainee.ssp.mock.servlets.BlockingServlet;
import com.erlanmomo.grpc.trainee.ssp.mock.servlets.NonBlockingServlet;
import com.erlanmomo.grpc.trainee.ssp.mock.utils.BidResponseService;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

public class JettyServer {
    private QueuedThreadPool queuedThreadPool;
    private Server server;

    public JettyServer(QueuedThreadPool threadPool) {
        this.queuedThreadPool = threadPool;
        new BidResponseService();
    }

    public void start() throws Exception {
        server = new Server(queuedThreadPool);
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8123);
        server.setConnectors(new Connector[]{connector});
        configure();
        server.start();
    }

    private void configure(){
        ServletHandler servletHandler = new ServletHandler();
        server.setHandler(servletHandler);
        servletHandler.addServletWithMapping(BlockingServlet.class, "/status");
        servletHandler.addServletWithMapping(NonBlockingServlet.class, "/async");
        servletHandler.addServletWithMapping(AdRequestServlet.class, "/postjson");
    }
}
