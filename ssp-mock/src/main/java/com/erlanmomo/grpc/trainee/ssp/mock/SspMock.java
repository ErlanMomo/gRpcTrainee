package com.erlanmomo.grpc.trainee.ssp.mock;

import com.erlanmomo.grpc.trainee.ssp.mock.server.JettyServer;
import com.erlanmomo.grpc.trainee.ssp.mock.utils.BidResponseService;
import org.eclipse.jetty.util.thread.QueuedThreadPool;

public class SspMock {
    public static void main(String[] args) {
        int maxThreads = 10;
        int minThreads = 1;
        int idleTimeout = 1;

        QueuedThreadPool threadPool = new QueuedThreadPool(maxThreads, minThreads, idleTimeout);

        JettyServer server = new JettyServer(threadPool);
        try {
            server.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
