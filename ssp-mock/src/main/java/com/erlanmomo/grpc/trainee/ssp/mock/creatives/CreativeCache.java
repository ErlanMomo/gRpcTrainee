package com.erlanmomo.grpc.trainee.ssp.mock.creatives;

import com.erlanmomo.grpc.trainee.common.entities.Creative;

public interface CreativeCache {
    String hash(Creative creative);
    Creative getByHash(String hash);
    Creative get(String crid, String tag);
}
