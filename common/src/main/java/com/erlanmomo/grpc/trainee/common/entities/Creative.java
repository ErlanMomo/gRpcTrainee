package com.erlanmomo.grpc.trainee.common.entities;

import java.util.Objects;

public class Creative {
    private Long id;
    private String hash;
    private String crid;
    private String status;
    private String tag;

    public Creative() {
    }

    public Creative(Long id, String crid, String status, String tag) {
        this.id = id;
        this.crid = crid;
        this.status = status;
        this.tag = tag;
    }

    public Creative(String crid, String status, String tag) {
        this.crid = crid;
        this.status = status;
        this.tag = tag;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getCrid() {
        return crid;
    }

    public void setCrid(String crid) {
        this.crid = crid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    @Override
    public String toString() {
        return "Creative{" +
                "id=" + id +
                ", hash='" + hash + '\'' +
                ", crid='" + crid + '\'' +
                ", status='" + status + '\'' +
                ", tag='" + tag + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Creative creative = (Creative) o;
        return Objects.equals(id, creative.id) &&
                Objects.equals(hash, creative.hash) &&
                Objects.equals(crid, creative.crid) &&
                Objects.equals(status, creative.status) &&
                Objects.equals(tag, creative.tag);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, hash, crid, status, tag);
    }
}
