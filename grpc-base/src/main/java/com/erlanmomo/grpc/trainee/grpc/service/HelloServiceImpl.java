package com.erlanmomo.grpc.trainee.grpc.service;

import com.erlanmomo.grpc.trainee.grpc.HelloRequest;
import com.erlanmomo.grpc.trainee.grpc.HelloResponse;
import com.erlanmomo.grpc.trainee.grpc.HelloServiceGrpc;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HelloServiceImpl extends HelloServiceGrpc.HelloServiceImplBase {
    private static final Logger LOGGER = LoggerFactory.getLogger(HelloServiceImpl.class);

    public HelloServiceImpl() {
        LOGGER.info("Service created");
    }

    @Override
    public void hello(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
        String greeting = "Hello, " +
                request.getFirstName() +
                " " +
                request.getLastName();

        HelloResponse response = HelloResponse.newBuilder()
                .setGreeting(greeting)
                .build();

        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }

    @Override
    public StreamObserver<HelloRequest> helloStream(StreamObserver<HelloResponse> responseObserver) {
        LOGGER.info("Stream start");
        return new StreamObserver<>() {
            @Override
            public void onNext(HelloRequest helloRequest) {
                LOGGER.info("On next");
                HelloResponse response = HelloResponse.newBuilder()
                        .setGreeting("Next hello " +
                                helloRequest.getFirstName() +
                                " " +
                                helloRequest.getLastName() +
                                "!")
                        .build();
                responseObserver.onNext(response);
            }

            @Override
            public void onError(Throwable throwable) {
                LOGGER.error("Unexpected error on stream ", throwable);
            }

            @Override
            public void onCompleted() {
                LOGGER.info("Complete");
                responseObserver.onCompleted();
            }
        };
    }
}
