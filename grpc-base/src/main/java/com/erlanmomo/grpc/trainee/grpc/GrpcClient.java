package com.erlanmomo.grpc.trainee.grpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CountDownLatch;

public class GrpcClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(GrpcClient.class);

    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder
                .forAddress("localhost", 9090)
                .usePlaintext()
                .build();

        helloStream(channel);
//        hello(channel);
//        cat(channel);

        channel.shutdown();
    }

    private static void cat(ManagedChannel channel) {
        CreativeServiceCatGrpc.CreativeServiceCatBlockingStub stub =
                CreativeServiceCatGrpc.newBlockingStub(channel);

        Response response = stub.onNewCreative(CreativeGrpc
                .newBuilder()
                .setTag("tag1")
                .setCrid("crid1")
                .build());
        LOGGER.info("Grpc client call finished");
    }

    private static void hello(ManagedChannel channel) {
        HelloServiceGrpc.HelloServiceBlockingStub stub =
                HelloServiceGrpc.newBlockingStub(channel);

        HelloResponse helloResponse = stub.hello(HelloRequest.newBuilder()
                .setFirstName("Ave")
                .setLastName("Emperor")
                .build());

        System.out.println(helloResponse);
    }

    private static void helloStream(ManagedChannel channel) {
        HelloServiceGrpc.HelloServiceStub stub = HelloServiceGrpc.newStub(channel);

//        CountDownLatch latch = new CountDownLatch(1);
        StreamObserver<HelloRequest> requestStreamObserver = stub.helloStream(new StreamObserver<>() {
            @Override
            public void onNext(HelloResponse helloResponse) {
                LOGGER.info("Got a response from server {}", helloResponse);
            }

            @Override
            public void onError(Throwable throwable) {
                LOGGER.error("Unexpected error on streaming ", throwable);
            }

            @Override
            public void onCompleted() {
                LOGGER.info("Finish work");
//                latch.countDown();
            }
        });

        LOGGER.debug("First call");
        requestStreamObserver.onNext(HelloRequest.newBuilder()
                .setFirstName("First name")
                .setLastName("Last name")
                .build());
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (true)
            throw new RuntimeException("AAAAAAAAAAAAAAAA");
        LOGGER.debug("Second call");
        requestStreamObserver.onNext(HelloRequest.newBuilder()
                .setFirstName("First name 2")
                .setLastName("Last name 2")
                .build());

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        requestStreamObserver.onCompleted();
        LOGGER.info("Method end");
    }
}
