package com.erlanmomo.grpc.trainee.cat.mock.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class CreativeUpdateJob {
    private static final Logger LOGGER =
            LoggerFactory.getLogger(CreativeUpdateJob.class);
    private static final String VALID_STATUS = "VALID";
    private CreativeService service;
    private CreativeObserver observer;

    private static final ScheduledExecutorService EXECUTOR =
            Executors.newSingleThreadScheduledExecutor();

    public CreativeUpdateJob(CreativeService service, CreativeObserver observer) {
        this.service = service;
        this.observer = observer;

        EXECUTOR.scheduleWithFixedDelay(
                this::job,
                0,
                10,
                TimeUnit.SECONDS
        );
    }

    private void job() {
        LOGGER.info("Update creative status job start");
        try {
            service.getAllCreatives()
                    .stream()
                    .filter(creative -> !VALID_STATUS.equals(creative.getStatus()))
                    .peek(creative -> LOGGER.info("Update creative status for creative {}", creative))
                    .forEach(creative -> {
                        service.updateCreativeStatus(creative, VALID_STATUS);
                        this.observer.onCreativeStatusChange(creative, VALID_STATUS);
                    });
        } catch (Throwable e) {
            LOGGER.error("Unexpected error on creative update status job", e);
        }
        LOGGER.info("Update creative status job finish");
    }
}
