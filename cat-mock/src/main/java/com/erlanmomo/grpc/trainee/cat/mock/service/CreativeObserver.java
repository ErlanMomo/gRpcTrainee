package com.erlanmomo.grpc.trainee.cat.mock.service;

import com.erlanmomo.grpc.trainee.common.entities.Creative;

public interface CreativeObserver {
    void onNewCreative(Creative creative);
    void onCreativeStatusChange(Creative creative, String newStatus);
}
