package com.erlanmomo.grpc.trainee.cat.mock.repository.jdbc;

import com.erlanmomo.grpc.trainee.cat.mock.repository.CreativeRepository;
import com.erlanmomo.grpc.trainee.common.entities.Creative;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

public class JdbcCreativeRepository implements CreativeRepository {
    private static final String TABLE_NAME = "creatives";
    private static final String SELECT_BY_ID = "select id, crid, status, tag from " +
            TABLE_NAME +
            " where id=?";
    private static final String SELECT_BY_CRID = "select id, crid, status, tag from " +
            TABLE_NAME +
            " where crid=?";
    private static final String SELECT_ALL = "select id, crid, status, tag from " +
            TABLE_NAME;
    private static final String INSERT = "insert into " + TABLE_NAME +
            "(crid, status, tag)" +
            "values(?,?,?)";
    private static final String UPDATE = "update " + TABLE_NAME +
            " set " +
            "crid = ?, " +
            "status = ?, " +
            "tag = ? " +
            "where id=?";

    @Override
    public Creative save(Creative creative) {
        if (creative.getId() != null || creative.getCrid() != null) {
            Creative storedCreative = null;
            if (creative.getId() != null) {
                storedCreative = findOne(creative.getId());
            }
            if (storedCreative != null) {
                try {
                    PreparedStatement statement = JdbcConfiguration.getConnection()
                            .prepareStatement(UPDATE);
                    statement.setString(1, creative.getCrid());
                    statement.setString(2, creative.getStatus());
                    statement.setString(3, creative.getTag());
                    statement.setLong(4, creative.getId());
                    statement.executeUpdate();
                    return creative;
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            PreparedStatement statement = JdbcConfiguration.getConnection()
                    .prepareStatement(INSERT);
            statement.setString(1, creative.getCrid());
            statement.setString(2, creative.getStatus());
            statement.setString(3, creative.getTag());
            int insertedRow = statement.executeUpdate();
            if (insertedRow != 1) {
                throw new RuntimeException("No rows inserted!");
            }
            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            creative.setId(generatedKeys.getLong(1));
            return creative;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Creative findOne(Long id) {
        try {
            PreparedStatement statement = JdbcConfiguration.getConnection()
                    .prepareStatement(SELECT_BY_ID);
            statement.setLong(1, id);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();
            String crid = resultSet.getString("crid");
            String status = resultSet.getString("status");
            String tag = resultSet.getString("tag");
            return new Creative(id, crid, status, tag);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Collection<Creative> findAll() {
        Collection<Creative> creatives = new LinkedList<>();
        try {
            Statement statement = JdbcConfiguration.getConnection()
                    .createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_ALL);
            while (resultSet.next()) {
                Long id = resultSet.getLong("id");
                String crid = resultSet.getString("crid");
                String status = resultSet.getString("status");
                String tag = resultSet.getString("tag");
                creatives.add(new Creative(id, crid, status, tag));
            }
            return creatives;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    @Override
    public Creative findByCrid(String crid) {
        try {
            PreparedStatement statement = JdbcConfiguration.getConnection()
                    .prepareStatement(SELECT_BY_CRID);
            statement.setString(1, crid);
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            resultSet.next();
            Long id = resultSet.getLong("id");
            String status = resultSet.getString("status");
            String tag = resultSet.getString("tag");
            return new Creative(id, crid, status, tag);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
