package com.erlanmomo.grpc.trainee.cat.mock.service;

import com.erlanmomo.grpc.trainee.cat.mock.repository.CreativeRepository;
import com.erlanmomo.grpc.trainee.cat.mock.repository.jdbc.JdbcCreativeRepository;
import com.erlanmomo.grpc.trainee.common.entities.Creative;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;

public class CreativeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(CreativeService.class);
    private CreativeRepository repository;

    public CreativeService() {
        this.repository = new JdbcCreativeRepository();
    }

    public Creative getCreative(Long id) {
        return this.repository.findOne(id);
    }

    public Creative saveCreative(Creative creative) {
        return repository.save(creative);
    }

    public void updateCreativeStatus(Creative creative, String status) {
        creative.setStatus(status);
        repository.save(creative);
    }

    public Collection<Creative> getAllCreatives(){
        return repository.findAll();
    }

}
