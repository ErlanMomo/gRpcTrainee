package com.erlanmomo.grpc.trainee.cat.mock.service.impl.grpc;

import com.erlanmomo.grpc.trainee.cat.mock.service.CreativeObserver;
import com.erlanmomo.grpc.trainee.cat.mock.service.CreativeService;
import com.erlanmomo.grpc.trainee.common.entities.Creative;
import com.erlanmomo.grpc.trainee.grpc.CreativeGrpc;
import com.erlanmomo.grpc.trainee.grpc.CreativeServiceCatGrpc;
import com.erlanmomo.grpc.trainee.grpc.CreativeStatusUpdate;
import com.erlanmomo.grpc.trainee.grpc.Request;
import com.erlanmomo.grpc.trainee.grpc.Response;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public class GrpcCreativeObserver
        extends CreativeServiceCatGrpc.CreativeServiceCatImplBase
        implements CreativeObserver {
    private static final Logger LOGGER = LoggerFactory.getLogger(GrpcCreativeObserver.class);
    private CreativeService service;
    private List<StreamObserver<CreativeStatusUpdate>> observerList = new ArrayList<>();

    public GrpcCreativeObserver(CreativeService service) {
        this.service = service;
        Server server = ServerBuilder
                .forPort(9090)
                .addService(this)
                .build();

        CompletableFuture.runAsync(() -> {
            try {
                LOGGER.info("GRPC server starting ");
                server.start();
                server.awaitTermination();
                LOGGER.info("End starting ");
            } catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
        });
    }

    @Override
    public void onNewCreative(Creative creative) {
        service.saveCreative(creative);
    }

    @Override
    public void onCreativeStatusChange(Creative creative, String newStatus) {
        observerList.forEach(observer ->
                observer.onNext(CreativeStatusUpdate
                        .newBuilder()
                        .setCrid(creative.getCrid())
                        .setStatus(newStatus)
                        .build()));
    }

    @Override
    public void onNewCreative(CreativeGrpc request, StreamObserver<Response> responseObserver) {
        LOGGER.info("Grpc CALL: onNewCreative");
        Creative creative = new Creative(request.getCrid(), request.getStatus(), request.getTag());
        this.onNewCreative(creative);
        LOGGER.info("Observer finish work");
        responseObserver.onNext(Response.newBuilder().build());
        responseObserver.onCompleted();
        LOGGER.info("Grpc CALL Completed");
    }

    @Override
    public void getAllCreatives(Request request, StreamObserver<CreativeGrpc> responseObserver) {
        LOGGER.info("Grpc CALL: getAllCreatives");
        service.getAllCreatives()
                .stream()
                .map(creative -> CreativeGrpc
                        .newBuilder()
                        .setCrid(creative.getCrid())
                        .setStatus(creative.getStatus())
                        .setTag(creative.getTag())
                        .build())
                .forEach(responseObserver::onNext);
        responseObserver.onCompleted();
    }

    @Override
    public void subscribeCreativeStatusUpdate(Request request, StreamObserver<CreativeStatusUpdate> responseObserver) {
        observerList.add(responseObserver);
    }
}
