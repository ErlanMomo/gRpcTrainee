package com.erlanmomo.grpc.trainee.cat.mock;

import com.erlanmomo.grpc.trainee.cat.mock.service.CreativeService;
import com.erlanmomo.grpc.trainee.cat.mock.service.CreativeUpdateJob;
import com.erlanmomo.grpc.trainee.cat.mock.service.impl.grpc.GrpcCreativeObserver;

public class CreativeAuditMock {
    public static void main(String[] args) {
        CreativeService service = new CreativeService();
        GrpcCreativeObserver observer = new GrpcCreativeObserver(service);
        new CreativeUpdateJob(service, observer);
    }
}

