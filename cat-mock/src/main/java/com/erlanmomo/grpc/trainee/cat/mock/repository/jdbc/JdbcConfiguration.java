package com.erlanmomo.grpc.trainee.cat.mock.repository.jdbc;

import org.postgresql.Driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JdbcConfiguration {
    private static final String DRIVER_CLASS_NAME = "org.postgresql.Driver";
    private static final String DATABASE_URL = "jdbc:postgresql://localhost:5432/erlan";
    private static final String DATABASE_USER = "test";
    private static final String DATABASE_PASSWORD = "test";

    private static Connection connection = connection();

    private static Connection connection() {
        try {
            Class.forName(DRIVER_CLASS_NAME);
            DriverManager.registerDriver(new Driver());
            Connection connection = DriverManager
                    .getConnection(DATABASE_URL, DATABASE_USER, DATABASE_PASSWORD);
            return connection;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Connection getConnection(){
        return connection;
    }
}
