package com.erlanmomo.grpc.trainee.cat.mock.repository;


import com.erlanmomo.grpc.trainee.common.entities.Creative;

import java.util.Collection;

public interface CreativeRepository {
    Creative save(Creative creative);
    Creative findOne(Long id);
    Collection<Creative> findAll();
    Creative findByCrid(String crid);
}
